#include <stdio.h>
#include <math.h>

#define N 720
#define PI 3.1415926536
#define PI2N (PI/(2*N))
#define LUT_FILENAME        "LUT.txt"
#define LUTm_FILENAME       "LUTm.txt"
#define LUTb_FILENAME       "LUTb.txt"

void generate_LUT(float LUT[])
{
    FILE *filePtr;
    filePtr = fopen(LUT_FILENAME,"w");
    fprintf(filePtr, "float LUT[%d] = {\n", N);
    for(int i = 0; i < N; i++)
    {
        double theta = i * PI2N;
        LUT[i] = tan(theta);
        fprintf(filePtr, "\t%.5g,\n", LUT[i]);
    }
    fprintf(filePtr, "};\n");
}

void generate_LUTm(float LUTm[], float LUT[])
{
    FILE *filePtr;
    filePtr = fopen(LUTm_FILENAME,"w");
    fprintf(filePtr, "float LUTm[%d] = {\n", N);
    for(int i = 0; i < N-1; i++)
    {
        LUTm[i] = PI2N/(LUT[i+1] - LUT[i]);
        fprintf(filePtr, "\t%.5g,\n", LUTm[i]);
    }
    LUTm[N-1] = 0;
    fprintf(filePtr, "\t%.5g\n", LUTm[N-1]);
    fprintf(filePtr, "};\n");
}


void generate_LUTb(float LUTb[], float LUT[])
{
    FILE *filePtr;
    filePtr = fopen(LUTb_FILENAME,"w");
    fprintf(filePtr, "float LUTb[%d] = {\n", N);
    for(int i = 0; i < N-1; i++)
    {
        LUTb[i] = PI2N * (i * LUT[i+1] - (i+1)*LUT[i]) / (LUT[i+1] - LUT[i]);
        fprintf(filePtr, "\t%.5g,\n", LUTb[i]);
    }
    LUTb[N-1] = PI/2;
    fprintf(filePtr, "\t%.5g\n", LUTb[N-1]);
    fprintf(filePtr, "};\n");
}

float get_angle(float x, float LUT[], float LUTm[], float LUTb[])
{
    int i;
    for(i = 1; i < N && x > LUT[i]; i++);
    if(x > LUT[i]) i--;
    return (LUTm[i] * x + LUTb[i]);
}

int main()
{
    float LUT[N];
    float LUTm[N];
    float LUTb[N];

    generate_LUT(LUT);
    generate_LUTm(LUTm, LUT);
    generate_LUTb(LUTb, LUT);

    float angle = get_angle((float)(1254.0/232.0), LUT, LUTm, LUTb) * 180 / PI;
    printf("%.3lf\n", angle);

    return 0;
}